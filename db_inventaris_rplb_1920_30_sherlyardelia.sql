-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2021 at 09:58 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris_sherly_ukom`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_pinjam_barang` (IN `id_peminjam` CHAR(8), IN `id_barang` CHAR(8), IN `jml_barang` INT(3))  BEGIN 
START TRANSACTION;
INSERT INTO pinjam_barang(peminjam, tgl_pinjam, barang_pinjam, jml_pinjam, kondisi) VALUES (id_peminjam, NOW(), id_barang, jml_barang, 'Baik');
UPDATE stok SET stok.total_barang = (stok.total_barang-jml_barang) WHERE stok.id_barang=id_barang;
UPDATE barang INNER JOIN stok ON barang.id_barang=stok.id_barang SET barang.jumlah_barang=stok.total_barang WHERE barang.id_barang=id_barang;
COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_pinjam_barang` (`id_pinjam` CHAR(8))  BEGIN 
DECLARE id_barang VARCHAR(8) DEFAULT "BRG00000";
SELECT pinjam_barang.barang_pinjam INTO id_barang FROM pinjam_barang WHERE pinjam_barang.id_pinjam=id_pinjam;
UPDATE pinjam_barang SET pinjam_barang.tgl_kembali=NOW() WHERE pinjam_barang.id_pinjam=id_pinjam;
UPDATE stok INNER JOIN pinjam_barang ON stok.id_barang=id_barang SET stok.total_barang = (stok.total_barang+pinjam_barang.jml_pinjam) WHERE stok.id_barang=id_barang;
UPDATE barang INNER JOIN stok ON barang.id_barang=id_barang SET barang.jumlah_barang=stok.total_barang WHERE barang.id_barang=id_barang;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `newkodeuser` () RETURNS CHAR(8) CHARSET utf8mb4 BEGIN
DECLARE kode_lama VARCHAR(8) DEFAULT "USR00000";
DECLARE ambil_angka CHAR(5) DEFAULT '00000';
DECLARE kode_baru VARCHAR(8) DEFAULT "USR00000";
SELECT MAX(user.id_user) INTO kode_lama FROM user;
SET ambil_angka = REGEXP_SUBSTR(kode_lama,"[0-9]+");
SET ambil_angka = ambil_angka + 1;
SET ambil_angka = LPAD(ambil_angka, 5, '0');
SET kode_baru = CONCAT("USR",ambil_angka);
RETURN kode_baru;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` char(8) NOT NULL,
  `nama_barang` varchar(225) NOT NULL,
  `spesifikasi` text NOT NULL,
  `lokasi` char(4) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `sumber_dana` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `spesifikasi`, `lokasi`, `kondisi`, `jumlah_barang`, `sumber_dana`) VALUES
('BRG10001', 'Kursi Siswa', 'Bantalan Merah\r\nAlumunium', 'R001', 'Baik', 35, 'S001'),
('BRG10002', 'Kursi Lipat Siswa', 'Kursi Lipat\r\nMerk Informa\r\nBantalan Hitam', 'R002', 'Baik', 36, 'S001'),
('BRG10003', 'Meja Siswa', 'Meja Alumunium Plastik', 'R003', 'Baik', 34, 'S005'),
('BRG20001', 'Laptop Acer Aspire E1-471', 'Acer Aspire E1-471\r\nCore i3, RAM 4GB, HDD 500GB', 'R002', 'Baik', 31, 'S002'),
('BRG20002', 'Laptop Lenovo E550', 'Laptop Lenovo E550\r\nIntel Core i7, RAM 8GB, HDD 1TB', 'R002', 'Baik', 34, 'S003'),
('BRG20003', 'PC Rakitan i7', 'Intel Core i7, \r\nRAM 16GB, \r\nSSD 512GB', 'R001', 'Baik', 12, 'S004'),
('BRG20004', 'Camera DSLR D60', 'DSLR Canon D60', 'R005', 'Baik', 16, 'S003'),
('BRG30001', 'Lighting Set', 'stand light tronik 2\r\nlighting tronik 2 100watt\r\n', 'R005', 'Baik', 1, 'S005'),
('BRG30002', 'Tripod Kamera', 'Takara Tripod', 'R005', 'Baik', 34, 'S002'),
('BRG30003', 'Keyboard Jellybean Round', '', 'R002', 'Baik', 8, 'S002');

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barang` char(8) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `jml_keluar` int(11) NOT NULL,
  `supplier` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barang`, `tgl_keluar`, `jml_keluar`, `supplier`) VALUES
('BRG10001', '2020-11-03', 16, 'SPR001'),
('BRG20001', '2017-11-06', 3, 'SPR005');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barang` char(8) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jml_masuk` int(11) NOT NULL,
  `supplier` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barang`, `tgl_masuk`, `jml_masuk`, `supplier`) VALUES
('BRG10001', '2007-08-01', 36, 'SPR001'),
('BRG10002', '2007-08-01', 36, 'SPR002'),
('BRG10003', '2021-11-10', 36, 'SPR002'),
('BRG20001', '2013-07-09', 30, 'SPR004'),
('BRG20002', '2014-03-08', 23, 'SPR003'),
('BRG20003', '2020-11-10', 12, 'SPR004'),
('BRG20004', '2014-04-13', 16, 'SPR005'),
('BRG30001', '2018-04-06', 2, 'SPR005'),
('BRG30002', '2018-04-06', 4, 'SPR005');

-- --------------------------------------------------------

--
-- Table structure for table `level_user`
--

CREATE TABLE `level_user` (
  `id_level` char(3) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level_user`
--

INSERT INTO `level_user` (`id_level`, `nama`, `keterangan`) VALUES
('U01', 'Administrator', ''),
('U02', 'Manajemen', ''),
('U03', 'Peminjam', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `listuser`
-- (See below for the actual view)
--
CREATE TABLE `listuser` (
`id_user` char(8)
,`foto_profil` varchar(225)
,`nama` varchar(225)
,`username` varchar(50)
,`password` text
,`level` varchar(225)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `list_user`
-- (See below for the actual view)
--
CREATE TABLE `list_user` (
`foto_profil` varchar(225)
,`nama` varchar(225)
,`username` varchar(50)
,`level` varchar(225)
);

-- --------------------------------------------------------

--
-- Table structure for table `log_peminjaman`
--

CREATE TABLE `log_peminjaman` (
  `id_pinjam` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `aksi` varchar(225) NOT NULL,
  `keterangan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_peminjaman`
--

INSERT INTO `log_peminjaman` (`id_pinjam`, `tanggal`, `aksi`, `keterangan`) VALUES
(14, '2021-11-19 16:40:07', 'Menambahkan', 'Menambahkan Peminjaman'),
(13, '2021-11-19 18:07:31', 'Mengembalikan', 'Barang Dikembalikan'),
(14, '2021-11-19 16:40:07', 'Menambahkan', 'Menambahkan Peminjaman'),
(13, '2021-11-19 18:07:31', 'Mengembalikan', 'Barang Dikembalikan'),
(29, '2021-11-24 15:57:15', 'Menambahkan', 'Menambahkan Peminjaman'),
(30, '2021-11-24 15:58:06', 'Menambahkan', 'Menambahkan Peminjaman');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` char(4) NOT NULL,
  `nama_lokasi` varchar(225) NOT NULL,
  `penanggung_jawab` varchar(225) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `nama_lokasi`, `penanggung_jawab`, `keterangan`) VALUES
('R001', 'Lab RPL 1', 'Satria Ade Putra', 'Lantai 3'),
('R002', 'Lab RPL 2', 'Satria Ade Putra', 'Lantai 3'),
('R003', 'Lab TKJ 1', 'Supriyadi', 'Lantai 2 Gedung D '),
('R004', 'Lab TKJ 2', 'Supriyadi', 'Lantai 2 Gedung D'),
('R005', 'Lab Multimedia', 'Bayu Setiawan', 'Gedung Multimedia');

-- --------------------------------------------------------

--
-- Table structure for table `pinjam_barang`
--

CREATE TABLE `pinjam_barang` (
  `id_pinjam` int(11) NOT NULL,
  `peminjam` char(8) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `barang_pinjam` char(8) NOT NULL,
  `jml_pinjam` int(11) NOT NULL,
  `tgl_kembali` date NOT NULL,
  `kondisi` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pinjam_barang`
--

INSERT INTO `pinjam_barang` (`id_pinjam`, `peminjam`, `tgl_pinjam`, `barang_pinjam`, `jml_pinjam`, `tgl_kembali`, `kondisi`) VALUES
(13, 'USR00001', '2021-11-19', 'BRG10001', 4, '2021-11-19', 'Baik'),
(14, 'USR00001', '2020-12-08', 'BRG10001', 2, '0000-00-00', 'baik'),
(29, 'USR00001', '2021-11-24', 'BRG30001', 3, '0000-00-00', 'Baik'),
(30, 'USR20006', '2021-11-24', 'BRG30003', 1, '0000-00-00', 'Baik');

--
-- Triggers `pinjam_barang`
--
DELIMITER $$
CREATE TRIGGER `triger_log_insert_pinjam` AFTER INSERT ON `pinjam_barang` FOR EACH ROW BEGIN
	INSERT INTO log_peminjaman (id_pinjam, tanggal, aksi, keterangan) 
    VALUES(new.id_pinjam, now(), 'Menambahkan', 'Menambahkan Peminjaman');
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `triger_log_update_pinjam` AFTER UPDATE ON `pinjam_barang` FOR EACH ROW BEGIN
	INSERT INTO log_peminjaman (id_pinjam, tanggal, aksi, keterangan) VALUES(new.id_pinjam, now(), 'Mengembalikan', 'Barang Dikembalikan');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_barang` char(8) NOT NULL,
  `jml_masuk` int(11) NOT NULL,
  `jml_keluar` int(11) NOT NULL,
  `total_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`id_barang`, `jml_masuk`, `jml_keluar`, `total_barang`) VALUES
('BRG10001', 36, 0, 35),
('BRG10002', 36, 16, 20),
('BRG10003', 36, 0, 37),
('BRG20001', 30, 3, 27),
('BRG20002', 23, 0, 28),
('BRG20003', 12, 0, 12),
('BRG20004', 16, 0, 19),
('BRG30001', 2, 0, 1),
('BRG30002', 4, 0, 8),
('BRG30003', 10, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `sumber_dana`
--

CREATE TABLE `sumber_dana` (
  `id_sumber` char(4) NOT NULL,
  `nama_sumber` varchar(225) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sumber_dana`
--

INSERT INTO `sumber_dana` (`id_sumber`, `nama_sumber`, `keterangan`) VALUES
('S001', 'Komite 07/09', 'Bantuan Komite 2007/2009'),
('S002', 'Komite 13', 'bantuan Komite 2013'),
('S003', 'Sed t-vet', 'Bantuan Kerja sama Indonesia Jerman'),
('S004', 'BOPD 2020', 'Bantuan Provinsi Jawa Barat'),
('S005', 'BOSDA 2018', 'Bantuan Operasional Sekolah Daerah Jawa Barat 2018');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` varchar(8) NOT NULL,
  `nama_supplier` varchar(225) NOT NULL,
  `alamat_supplier` text NOT NULL,
  `telp_supplier` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat_supplier`, `telp_supplier`) VALUES
('SPR001', 'INFORMA-MALL METROPOLITAN BEKASI', 'Mall Metropolitan, Jl. KH. Noer Ali No.1, RT.008/RW.002, Pekayon Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17148', '0812-9604-6051'),
('SPR002', 'Mitrakantor.com', 'Jl. I Gusti Ngurah Rai No.20, RT.1/RW.10, Klender, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta', '(021) 22862086'),
('SPR003', 'Bhinneka.com', 'Jl. Gn. Sahari No.73C, RT.9/RW.7, Gn. Sahari Sel., Kec. Kemayoran, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10610', '(021) 29292828'),
('SPR004', 'World Computer', 'Harco Mangga Dua Plaza, Jalan Arteri Jl. Mangga Dua Raya No.17, RW.11, Mangga Dua Sel., Kecamatan Sawah Besar, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10730', '(021) 6125266'),
('SPR005', 'Anekafoto Metro Atom', 'Metro Atom Plaza Jalan Samanhudi Blok AKS No. 19, RT.20/RW.3, Ps. Baru, Kecamatan Sawah Besar, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10710', '(021) 3455544');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` char(8) NOT NULL,
  `foto_profil` varchar(225) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `level` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `foto_profil`, `nama`, `username`, `password`, `level`) VALUES
('USR00001', '', 'Nana Sukmana', 'admin', '$2y$10$xzVEXybiD0fby0vk20aUwOh9NjDot9qKNdLfbX4TMzDscHnzd6pZ.', 'U03'),
('USR20006', '1637310300_e6a002ccb2c5e5b3d462.jpg', 'Deden Deendi', 'toolman=RPL', '$2y$10$/DQmZmEae0rTrM7pAT//C.3qwdCIbBs6MFLdUfvKDUCrsGtgO2hFi', 'U02'),
('USR20010', '1637320508_dce04563e1cf6d9c5dc2.jpg', 'Erik ', 'erik', '$2y$10$8fKxn8QYns48Pn/t5wiE8.fIX9GI4H77bF/EJJ/d./HyANXk7bhmS', 'U02'),
('USR20011', '1637320561_3f0c2f5e1588ce3e9d35.png', 'Farabi Mahardika', 'adyrlia', '$2y$10$KUYl33U/DYx0It9VIQWxe.9okU0QAqzCWx/xm5QNCx7ScSFgPyMJi', 'U02');

-- --------------------------------------------------------

--
-- Structure for view `listuser`
--
DROP TABLE IF EXISTS `listuser`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `listuser`  AS SELECT `a`.`id_user` AS `id_user`, `a`.`foto_profil` AS `foto_profil`, `a`.`nama` AS `nama`, `a`.`username` AS `username`, `a`.`password` AS `password`, `b`.`nama` AS `level` FROM (`user` `a` join `level_user` `b` on(`a`.`level` = `b`.`id_level`)) ;

-- --------------------------------------------------------

--
-- Structure for view `list_user`
--
DROP TABLE IF EXISTS `list_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `list_user`  AS SELECT `user`.`foto_profil` AS `foto_profil`, `user`.`nama` AS `nama`, `user`.`username` AS `username`, `level_user`.`nama` AS `level` FROM (`user` join `level_user` on(`level_user`.`id_level` = `user`.`level`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `lokasi` (`lokasi`),
  ADD KEY `sumber_dana` (`sumber_dana`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `level_user`
--
ALTER TABLE `level_user`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `log_peminjaman`
--
ALTER TABLE `log_peminjaman`
  ADD KEY `id_pinjam` (`id_pinjam`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `pinjam_barang`
--
ALTER TABLE `pinjam_barang`
  ADD PRIMARY KEY (`id_pinjam`),
  ADD KEY `barang_pinjam` (`barang_pinjam`),
  ADD KEY `peminjam` (`peminjam`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `sumber_dana`
--
ALTER TABLE `sumber_dana`
  ADD PRIMARY KEY (`id_sumber`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `user_ibfk_1` (`level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pinjam_barang`
--
ALTER TABLE `pinjam_barang`
  MODIFY `id_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`lokasi`) REFERENCES `lokasi` (`id_lokasi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`sumber_dana`) REFERENCES `sumber_dana` (`id_sumber`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD CONSTRAINT `barang_keluar_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_keluar_ibfk_2` FOREIGN KEY (`supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD CONSTRAINT `barang_masuk_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_masuk_ibfk_2` FOREIGN KEY (`supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log_peminjaman`
--
ALTER TABLE `log_peminjaman`
  ADD CONSTRAINT `log_peminjaman_ibfk_1` FOREIGN KEY (`id_pinjam`) REFERENCES `pinjam_barang` (`id_pinjam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pinjam_barang`
--
ALTER TABLE `pinjam_barang`
  ADD CONSTRAINT `pinjam_barang_ibfk_1` FOREIGN KEY (`barang_pinjam`) REFERENCES `barang` (`id_barang`),
  ADD CONSTRAINT `pinjam_barang_ibfk_2` FOREIGN KEY (`peminjam`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`level`) REFERENCES `level_user` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
