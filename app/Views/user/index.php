<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col">


            <h1 class="mt-2">User</h1>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end ">
                <a href="/user/create" class="btn btn-primary mt-2 b-nav">Tambah User</a>
            </div>
            </br>
            <div class="row">
                <?php if (session()->getFlashdata()) : ?>
                    <div class="alert alert-warning" role="alert">
                        <?= session()->getFlashdata('pesan'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <!-- Table -->
            <table class="table table-striped  align-middle">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Username</th>
                        <th scope="col">Level</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($user as $s) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td>
                                <img src="/img/<?= $s->foto_profil ?>" class="img-user" alt="...">
                            </td>
                            <td><?= $s->nama ?></td>
                            <td><?= $s->username ?></td>
                            <td><?= $s->level ?></td>
                            <td>
                                <button class="btn btn-sm">
                                    <a href="/user/hapus/<?= $s->id_user ?>">
                                        <img src="<?= base_url('img/delete.png') ?>" width="20px" />
                                    </a>
                                </button>
                                <button class="btn btn-sm">
                                    <a href="/user/edit/<?= $s->id_user ?>">
                                        <img src="https://img.icons8.com/ios-glyphs/15/000000/pencil--v1.png" />
                                    </a>
                                </button>
                                <!-- <button class="btn btn-sm">
                                    <a href="/user/detail/<?= $s->id_user ?>">
                                        <img src="https://img.icons8.com/ios-glyphs/15/000000/visible.png" />
                                    </a>
                                </button>
                                <button class="btn btn-sm">
                                    <img src="https://img.icons8.com/ios-glyphs/15/000000/pencil--v1.png" />
                                </button> -->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>