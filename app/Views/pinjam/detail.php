<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="mt-2">Detail Peminjaman</h2>
            <div class="card mb-20" style="max-width: 540px;">
                <div class="row no-glutters">
                    <dd($pinjam); ?>
                        <div class="col-md-12">
                            <div class="card-body">
                                <!-- <img src="/img/mejasiswa.jpg" width="80%" height="80%"> -->
                                <h5 class="card-title"><?= $barang['nama_barang']; ?></h5>
                                <p class="card-text"><b>User : </b><?= $user['nama']; ?></p>
                                <p class="card-text"><b>Jumlah Pinjam : </b><?= $pinjam['jml_pinjam']; ?></p>
                                <p class="card-text"><b>Tanggal Pinjam : </b><?= $pinjam['tgl_pinjam']; ?></p>
                                <p class="card-text"><b>Tanggal Kembali : </b><?= $pinjam['tgl_kembali']; ?></p>
                                <p class="card-text"><b>Kondisi : </b><?= $pinjam['kondisi']; ?></p>
                                <a href="/pinjam/edit/<?= $pinjam['id_pinjam']; ?>" class="btn btn-warning">Kembalikan</a>

                                <!-- <form action="<//?= base_url('/pinjam/delete/' . $pinjam['id_pinjam']); ?>" method="get" class="d-inline">
                                    <//?= csrf_field(); ?>
                                     //mengamankan supaya terhindar dari hacking
                                    <input type="hidden" name="_method" value="DELETE">  //name method itu spoofing nya
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('yakin ingin hapus data?');">Delete</button>
                                </form> -->

                                <br>
                                <br>
                                <a href="/pinjam">Kembali ke daftar peminjaman</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->endSection(); ?>